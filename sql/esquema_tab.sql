
{ TABLE "modula".empresas1 row size = 888 number of columns = 17 index size = 0 }

create table "modula".empresas1 
  (
    bodega smallint,
    nit char(15),
    razon_social char(70),
    nom_sucursal char(70),
    cod_sucursal char(3),
    direccion1 char(70),
    direccion2 char(70),
    depto char(20),
    municipio char(20),
    codigo_postal char(5),
    e_mail char(50),
    telefonos char(80),
    requestor char(40),
    userface char(20),
    url_services varchar(250),
    llave_archivo varchar(70),
    llave_pass varchar(30)
  );

revoke all on "modula".empresas1 from "public" as "modula";


{ TABLE "informix".empresas1_f row size = 4 number of columns = 2 index size = 0 }

create table "informix".empresas1_f 
  (
    tipo_frase smallint not null ,
    codigo_escenario smallint not null 
  );

revoke all on "informix".empresas1_f from "public" as "informix";


{ TABLE "pruebas".facturafel_e row size = 1461 number of columns = 60 index size = 260 }

create table "pruebas".facturafel_e 
  (
    serie char(20),
    num_doc decimal(15,0),
    tipod char(2),
    fecha date,
    fecha_em char(35),
    fecha_anul char(35),
    num_aut char(20),
    fecha_re char(15),
    rango_i decimal(12,0),
    rango_f decimal(12,0),
    from char(50),
    to char(50),
    cc char(50),
    formats char(10),
    tipo_doc char(12),
    estado_doc char(20),
    c_moneda char(3),
    tipo_cambio char(10),
    info_reg char(15),
    num_int decimal(20,0),
    nit_e char(12),
    nombre_c char(70),
    idioma_e char(2),
    nombre_e char(70),
    codigo_e char(3),
    dispositivo_e char(3),
    direccion_e char(70),
    departamento_e char(20),
    municipio_e char(20),
    pais_e char(2),
    codpos_e decimal(5,0),
    nit_r char(12),
    nombre_r char(70),
    idioma_r char(2),
    direccion_r char(70),
    departamento_r char(20),
    municipio_r char(20),
    pais_r char(2),
    codpos_r decimal(5,0),
    total_b decimal(10,2),
    total_d decimal(10,2),
    monto_d decimal(10,2),
    total_i decimal(10,2),
    ing_netosg decimal(10,2),
    total_iva1 decimal(10,2),
    tipo1 char(10),
    base1 decimal(10,2),
    tasa1 decimal(10,2),
    monto1 decimal(10,2),
    total_neto decimal(10,2),
    serie_e char(20),
    numdoc_e char(40),
    autorizacion char(200),
    estatus char(2),
    fac_id serial not null constraint "pruebas".n2092_117,
    p_descuento decimal(5,2),
    fel_msg integer,
    id_factura integer,
    total_en_letras char(250),
    tipo_pago char(2)
  );

revoke all on "pruebas".facturafel_e from "public" as "pruebas";


create unique index "pruebas".ix_fel_01 on "pruebas".facturafel_e 
    (fac_id) using btree ;
create index "pruebas".ix_fel_02 on "pruebas".facturafel_e (serie,
    num_doc,tipod,fecha) using btree ;
create index "pruebas".ix_fel_03 on "pruebas".facturafel_e (fecha,
    estatus,autorizacion) using btree ;


{ TABLE "pruebas".facturafel_ed row size = 249 number of columns = 23 index size = 9 }

create table "pruebas".facturafel_ed 
  (
    fac_id integer,
    serie char(20),
    num_doc decimal(15,0),
    tipod char(2),
    fecha date,
    descrip_p char(70),
    codigo_p char(14),
    unidad_m char(3),
    cantidad decimal(5,0),
    precio_u decimal(14,2),
    precio_t decimal(14,2),
    descto_t decimal(14,2),
    descto_m decimal(14,2),
    precio_p decimal(14,2),
    precio_m decimal(14,2),
    total_imp decimal(14,2),
    ing_netos decimal(14,2),
    total_iva decimal(14,2),
    tipo char(15),
    base decimal(14,2),
    tasa decimal(9,2),
    monto decimal(14,2),
    categoria char(10)
  );

revoke all on "pruebas".facturafel_ed from "public" as "pruebas";


create index "pruebas".ix_feled_01 on "pruebas".facturafel_ed 
    (fac_id) using btree ;


{ TABLE "informix".factura_log row size = 16509 number of columns = 26 index size = 0 }

create table "informix".factura_log 
  (
    correlativo integer not null ,
    request_id varchar(40) not null ,
    fac_id integer not null ,
    fecha_envio datetime year to fraction(3) not null ,
    fecha_finaliza datetime year to fraction(3),
    estatus smallint not null ,
    numero_acceso decimal(9,0),
    interna_tipod char(2) not null ,
    interna_serie varchar(15) not null ,
    interna_numero decimal(15,0) not null ,
    intentos smallint,
    tipo_respuesta smallint,
    xml_docto lvarchar(5120),
    xml_respuesta lvarchar(10240),
    cod_archivo_cer integer,
    dir_arch_org varchar(200),
    dir_arch_fir varchar(200),
    dir_arch_cer varchar(200),
    sat_uuid varchar(40),
    certificador_nit varchar(25),
    certificador_nombre varchar(80),
    certificador_serie char(8),
    certificador_numero decimal(15,0),
    certificador_fecha datetime year to fraction(3),
    flag_error smallint not null ,
    msg_error varchar(250)
  );

revoke all on "informix".factura_log from "public" as "informix";


{ TABLE "informix".factura_xml row size = 70 number of columns = 3 index size = 0 }

create table "informix".factura_xml 
  (
    cod_archivo_cer integer not null ,
    fecha_hora datetime year to fraction(3),
    archivo_xml text
  );

revoke all on "informix".factura_xml from "public" as "informix";

