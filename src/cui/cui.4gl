{
https://github.com/minfingt/validators/blob/master/src/Minfin.Validators/CuiValidator.cs
}

MAIN

DEFINE cui STRING 
DEFINE i SMALLINT 

    LET cui = arg_val(1)

    --Que no sea nulo
    IF cui IS NULL THEN
        DISPLAY "NIT es nulo"
        RETURN 
    END IF 

    --Que tenga 13 posiciones
    IF cui.getLength() <> 13 THEN
        DISPLAY "NIT no tiene 13 posiciones"
        RETURN 
    END IF 

    IF NOT esNumero(cui) THEN
        DISPLAY "Solo pueden ser numeros"
        RETURN 
    END IF 
END MAIN

FUNCTION esNumero(num STRING  )
DEFINE i SMALLINT 
    --Que sean numeros
    FOR i = 1 TO num.getLength()
        IF num.getCharAt(i) NOT MATCHES "[0-9]" THEN
            RETURN FALSE 
        END IF 
    END FOR 
    RETURN TRUE 
END FUNCTION 

