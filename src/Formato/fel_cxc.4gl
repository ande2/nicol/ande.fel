
report felfelr( prfelg)
	define
		cantletras char(150),
		cantletras2 char(150),
		v_valdol DECIMAL(15,2),
		val_totfac, val_subtot, valor1, valor2 DECIMAL(15,2),
		prfelg record like facturafel_e.*,
		prfeld record like facturafel_ed.*,
		relit1			char (2),
		relit0			char (2),

         rcomp1      CHAR(2),
         rcomp0      CHAR(2),
         r12xpul     CHAR(2),
         r10xpul     CHAR(2),
         r8xpul      CHAR(2),

		simbolo CHAR(3),
		vcantlet CHAR(150),
		conta1, conta2, conta3, conta4, longi SMALLINT,
		line1, line2 CHAR(100),
		pchar CHAR(1),
		vnit CHAR(20),
		ii SMALLINT,
		prndocu_desc RECORD LIKE ndocu_desc.*,
		vestado_doc CHAR(50),
		vcontroles CHAR(50)

	output
		left margin 0
		right margin 240
		top margin 1
		bottom margin 0
		#page length 70  era del formato anterior
		page length 88  #68 LINES formato orignal  88 tamano carta a 8lpi
		
	format

		first page header
			let relit1 = ascii 27, ascii 77
			let relit0 = ascii 27, ascii 80

        LET rcomp1 = ASCII 27, ASCII 15
        LET rcomp0 = ASCII 27, ASCII 18
        LET r8xpul  = ASCII 27, ASCII 48
        LET r12xpul = ASCII 27, ASCII 77
        LET r10xpul = ASCII 27, ASCII 80
        #LET rslaveon = ASCII 27, ASCII 91, ASCII 53, ASCII 105
        #LET rslaveoff = ASCII 27, ASCII 91, ASCII 52, ASCII 105


	ON EVERY ROW
		IF prfelg.tipo_cambio > 1 THEN
			LET simbolo = "$"
		ELSE
			LET simbolo = "Q."
		END IF


		SELECT estado_doc INTO vestado_doc
		FROM facturafel_e
		WHERE documlk = prfelg.documlk
		AND estado_doc = "ANULADO"
		AND estatus = "C"

		IF STATUS = NOTFOUND THEN
			LET vestado_doc = ""
		ELSE
			LET vestado_doc = "*** DOCUMENTO ANULADO ***"
		END IF

       PRINT r8xpul;
       PRINT r12xpul;
       PRINT ASCII 27, ASCII 69;

	#print ascic 27, ascii 48;  #8 lpi
	#PRINT PRINT ASCII 27, ASCII 64;
	#print ascii 18;  				#normal
	print ascii 27, ascii 71;  #doble strike

	skip 3 lines
	PRINT ASCII 15 
	PRINT COLUMN 8, "G4S DOCUMENTA, S.A."
	PRINT COLUMN 8, "NIT :  60010207";
	IF prfelg.tipod = 'FC' THEN
		PRINT COLUMN 120, "FACTURA CAMBIARIA ELECTRONICA"
	ELSE
		PRINT COLUMN 120, "NOTA DE CREDITO ELECTRONICA"
	END IF
	SKIP 1 LINES


	IF prfelg.autorizacion IS NULL THEN
		PRINT 
			 COLUMN 143, ""
		PRINT COLUMN 8, "*** PENDIENTE ***",
			 COLUMN 143, prfelg.fecha USING "dd/mm/yyyy"
	ELSE
		PRINT 
			 COLUMN 143, ""
		PRINT COLUMN 8, prfelg.autorizacion[1,50], 
			 COLUMN 143, prfelg.fecha USING "dd/mm/yyyy"
	END IF

	LET vcontroles = 	prfelg.documlk USING "#######", "-", 
							prfelg.fac_id  USING "#######"
	PRINT COLUMN 8, vestado_doc,
		 	COLUMN 143, prfelg.serie_e[1,12]

	PRINT COLUMN 143, prfelg.numdoc_e[1,14]

	PRINT COLUMN 8, prfelg.fecha_em[1,19],
	 		COLUMN 143, prfelg.num_interno[1,14] #documento original

	PRINT COLUMN 8, "4ta. avenida 14-65 zona 1",
			COLUMN 143, vcontroles CLIPPED

	PRINT 
	PRINT
	print	column 25, prfelg.nombre_r,
	 		column 140, prfelg.nit_r
	print column 25, prfelg.direccion_r[1,110]

	PRINT
	PRINT

	IF prfelg.tipo_pago = 'CO' THEN
		PRINT COLUMN 25, prfelg.vencod, COLUMN 70, prfelg.num_oc, 
		COLUMN 118, "XX", 		 #, COLUMN 75, simbolo;
		COLUMN 143, prfelg.clicod CLIPPED
	ELSE
		PRINT COLUMN 25, prfelg.vencod, COLUMN 70, prfelg.num_oc, 
		COLUMN 133, "xx", 		 #, COLUMN 75, simbolo;
		COLUMN 143, prfelg.clicod CLIPPED
	END IF

	skip 4 lines

	DECLARE detfel CURSOR FOR
	SELECT d.*
	FROM facturafel_ed d
	WHERE d.fac_id = prfelg.fac_id

	LET val_subtot = 0  LET contador = 0
   foreach detfel INTO prfeld.*
		{IF prfelg.tc > 1 THEN
			LET valor1 = (prandocumd[contador].precio / prdocg.tc)
			LET valor2 = (prandocumd[contador].valordet / prdocg.tc)

			LET val_subtot = val_subtot + valor2
		ELSE
			LET valor1 = prandocumd[contador].precio 
			LET valor2 = prandocumd[contador].valordet 
		END IF}

		LET contador = contador + 1

		IF prfeld.txt_especial = "N" THEN
      	PRINT COLUMN 5, 
			prfeld.cantidad USING "######", "      ", 
         prfeld.codigo_p,  " ", "     ", 
         prfeld.descrip_p[1,75], " ", 
	   	COLUMN 120, prfeld.precio_u USING "##,###,###.##", "  ", 
		   COLUMN 140, prfeld.precio_t USING "##,###,###.##"

		ELSE

			FOR ii = 1 TO 14

				SELECT * INTO prndocu_desc.*
				FROM ndocu_desc
				WHERE documlk = prfelg.documlk
				AND linea = ii

				IF STATUS = NOTFOUND THEN
					PRINT 

				ELSE
					IF ii = 1 THEN
      				PRINT COLUMN 5, 
						1 USING "######",   "      ", 
         			"               ",  "     ", 
         			prndocu_desc.descri, " ", 
	   				COLUMN 120, prfeld.precio_u USING "##,###,###.##", "  ", 
		   			COLUMN 140, prfeld.precio_t USING "##,###,###.##"

					ELSE
      				PRINT COLUMN 5, 
						"      ", "      ", 
         			"               ",  "     ", 
         			prndocu_desc.descri
					END IF

				END IF
				
			END FOR

			LET contador = ii
		END IF

  	END FOREACH

	PRINT

	IF prfelg.serie_planta IS NOT NULL THEN
		PRINT COLUMN 37, "SERIE     : ", prfelg.serie_planta
		PRINT COLUMN 37, "MOTOR     : ", prfelg.modelo
		PRINT COLUMN 37, "GENERADOR : ", prfelg.marca
	ELSE
		PRINT
		PRINT
		PRINT
	END IF

	PRINT COLUMN 37, prfelg.nota1

   FOR conta1 = (contador+1) TO 14
       #SKIP 1 LINE
		 PRINT COLUMN 37, " " #"-------"
   END FOR

	{IF prdocg.tc > 1 THEN
		#LET valor1 = prdocg.subtotal / prdocg.tc
		LET valor1 = val_subtot
		LET valor2 = (prdocg.descuento / prdocg.tc)

		LET val_totfac = val_subtot - valor2

		ERROR "SUBTOTAL:", valor1, " DESCTO: ", valor2, 
		" TOTAL FC: ", val_totfac
	ELSE
		LET valor1 = prdocg.subtotal 
		LET valor2 = prdocg.descuento
	END IF}

	SKIP 15 LINES

	IF prfelg.tipo_cambio > 1 THEN
		PRINT COLUMN 24, "TC. Q. ", prfelg.tipo_cambio using "#&.&&&&&",
		 COLUMN 60,   "US DOLAR",
		 COLUMN 137, simbolo, prfelg.total_d USING "##,###,###.##"

	ELSE
		PRINT COLUMN 60, "QUETZAL",
		 COLUMN 137, simbolo, prfelg.total_d USING "##,###,###.##"
	END IF


	{ del fuente original
	#CREATE TEMP TABLE tmp_letras(letras CHAR(100))
	IF prdocg.tc > 1 THEN
		LET v_valdol = val_totfac  #prdocg.saldo / prdocg.tc da diferencia
   	#CALL dol_numlet(v_valdol) RETURNING cantletras
   	CALL ndolnumlet(v_valdol) RETURNING cantletras
	ELSE
		LET v_valdol = prdocg.saldo 
   	#CALL numlet(prdocg.saldo) RETURNING cantletras
   	CALL nnumlet(prdocg.saldo) RETURNING cantletras
   	#CALL nnumlet(999999999) RETURNING cantletras #para test
	END IF
	#INSERT INTO tmp_letras VALUES (cantletras)
	#SELECT initcap(letras) INTO cantletras2 FROM tmp_letras
	#LET cantletras = cantletras2
	#DROP TABLE tmp_letras
}

		LET vcantlet = prfelg.total_en_letras
		LET line1 = NULL LET line2 = NULL
      LET longi=LENGTH(vcantlet)
	   FOR conta2 = 1 TO longi
	       IF vcantlet[conta2] = "," THEN
	          EXIT FOR
	       END IF
	   END FOR

	   LET line1 = vcantlet[1,(conta2-1)]
	   LET conta4 = 0

      FOR conta3 = conta2+1 TO longi
         LET conta4 = conta4 + 1
         LET line2[conta4] = vcantlet[conta3,conta3]
      END FOR

	print column 22, line1,
			column 137, simbolo, prfelg.mto_descuento using "##,###,###.##"
	print column 22, line2, 
	 		COLUMN 137, simbolo, prfelg.total_neto USING "##,###,###.##"

	SKIP 4 LINES
	PRINT COLUMN 60, prfelg.un_pago_de USING "###,###.##",
	 		COLUMN 77,  prfelg.npagos USING "###",
		COLUMN 122, prfelg.mto_pago USING "###,###.##", 
		COLUMN 139, prfelg.dia_pago USING "##"
	PRINT	COLUMN 8, prfelg.fecha_pago USING "dd/mm/yyyy"
	skip 10 LINES
	PRINT COLUMN 18, prfelg.nom_firma
	PRINT COLUMN 18, prfelg.dpi_firma

	#print ascii 15; 				#condensado
	#print ascii 27, ascii 50;	#6 lpi
	#print ascii 27, ascii 80;	#10 cpi
end report 
