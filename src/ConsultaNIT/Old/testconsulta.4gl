# https://fel.g4sdocumenta.com/ConsultaNIT/ConsultaNIT.asmx?WSDL

GLOBALS "g4snitconsulta.inc"

MAIN
   DEFINE result INT 

   --LET getNIT.Entity = '324272' perco 3712710        
   LET getNIT.Entity = '324272'
   --perco 4F59F4EB-4409-4D8E-8C70-4FFAA4C1C960    
   LET getNIT.Requestor = '4722F3AF-3FDE-44A9-A9F1-B767A4BEB17C'
   LET getNIT.vNIT = '91972124'

   LET result = getNIT_g()

   IF result = 0 THEN 
      DISPLAY "Resultado ", result, ' >>', getNitResponse.getNITResult.Response.nombre
   ELSE
      DISPLAY "Error ", wsError.code, " - ", wsError.description
   END IF 
   
END MAIN 