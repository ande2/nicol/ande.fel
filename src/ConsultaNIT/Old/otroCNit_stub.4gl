# https://fel.g4sdocumenta.com/ConsultaNIT/ConsultaNIT.asmx?WSDL

GLOBALS "otro.inc"

MAIN
   DEFINE result INT 

   --Codigo de usuario       
   LET getNIT.Entity = '3712710'
   --LLave
   LET getNIT.Requestor = '4F59F4EB-4409-4D8E-8C70-4FFAA4C1C960'
   
   LET getNIT.vNIT = '91972124'

   LET result = getNIT_g()

   IF result = 0 THEN 
      DISPLAY "Resultado ", result, ' >>', getNitResponse.getNITResult.Response.nombre
   ELSE
      DISPLAY "Error ", wsError.code, " - ", wsError.description
   END IF 
   
END MAIN 